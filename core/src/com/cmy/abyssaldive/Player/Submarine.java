package com.cmy.abyssaldive.Player;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.SubmarineGame;

public class Submarine {
    public static final int DEFAULT_WIDTH = 200;
    public static final int DEFAULT_HEIGHT = 100;
    public static final float DEFAULT_MOVEMENT_SPEED_X = 500f;
    public static final float DEFAULT_POS_X = (com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH >> 1) - (DEFAULT_WIDTH >> 1);
    public static final float DEFAULT_POS_Y = com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.80f;
    public static final float DEFAULT_HITBOX_SCALE = 0.9f;
    public static final float DEFAULT_BATTERY_DRAIN = 1.5f;
    public static final int SUBMARINE_TYPE_DEFAULT = 0;
    public static final int SUBMARINE_TYPE_NUCLEAR = 1;
    public static final int SUBMARINE_TYPE_ARMOURED = 2;
    public static final int MOVE_SUBMARINE_RIGHT = 1;
    public static final int MOVE_SUBMARINE_LEFT = -1;
    public static final int SUBMARINE_STOPPED = 0;
    public static final int DEFAULT_STARTING_LIFE = 3;
    public static final int STATE_DEAD = 0;
    public static final int STATE_ALIVE = 1;
    public static final int STATE_INVULNERABLE = 2;
    public float x;
    public float y;
    public float width;
    public float height;
    public int lifePoints;
    public Rectangle hitbox;
    public SubmarineLight submarineLight;
    public int state;
    public float battery = 100f;
    public int submarineType;
    private float movementSpeedX;
    private float hitboxScale;
    private float submarineAnimationState;
    private float animationSpeed;
    private Texture[] texture;
    //    private Texture textureInvulnerable01;
    private Texture textureInvulnerable;
    private com.cmy.abyssaldive.Assets assets;
    private float timeLeftInvulnerability;
    private float batteryDrain;

    public Submarine(com.cmy.abyssaldive.Assets assets, int submarineType) {
        this.assets = assets;
        x = DEFAULT_POS_X;
        y = DEFAULT_POS_Y;

        setSubmarineType(submarineType);


        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;

        hitboxScale = DEFAULT_HITBOX_SCALE;
        submarineAnimationState = 0;


//        textureInvulnerable01 = assets.manager.get(Assets.submarineInvulnerable01);
        textureInvulnerable = assets.manager.get(com.cmy.abyssaldive.Assets.submarineInvulnerable02);

        hitbox = new Rectangle(
                x + (1 - hitboxScale) * width / 2,
                y - height + (1 - hitboxScale) * height / 2,
                width * hitboxScale,
                height * hitboxScale
        );

        submarineLight = new SubmarineLight(assets, x, y, width, height, this);
        state = STATE_ALIVE;
    }

    public void setSubmarineType(int submarineType) {
        switch (submarineType) {
            default:
            case SUBMARINE_TYPE_DEFAULT:
                this.submarineType = SUBMARINE_TYPE_DEFAULT;
                movementSpeedX = DEFAULT_MOVEMENT_SPEED_X;
                animationSpeed = 3f;
                texture = new Texture[]{assets.manager.get(com.cmy.abyssaldive.Assets.submarine01), assets.manager.get(com.cmy.abyssaldive.Assets.submarine02), assets.manager.get(com.cmy.abyssaldive.Assets.submarine03), assets.manager.get(com.cmy.abyssaldive.Assets.submarine04)};
                lifePoints = DEFAULT_STARTING_LIFE;
                batteryDrain = DEFAULT_BATTERY_DRAIN;
                break;

            case SUBMARINE_TYPE_NUCLEAR:
                this.submarineType = SUBMARINE_TYPE_NUCLEAR;
                movementSpeedX = DEFAULT_MOVEMENT_SPEED_X * 1.5f;
                animationSpeed = 4f;
                texture = new Texture[]{assets.manager.get(com.cmy.abyssaldive.Assets.submarineNuclear01), assets.manager.get(com.cmy.abyssaldive.Assets.submarineNuclear02), assets.manager.get(com.cmy.abyssaldive.Assets.submarineNuclear03), assets.manager.get(com.cmy.abyssaldive.Assets.submarineNuclear04)};
                lifePoints = DEFAULT_STARTING_LIFE - 1;
                batteryDrain = (DEFAULT_BATTERY_DRAIN / 2f) * 0.75f;
                break;

            case SUBMARINE_TYPE_ARMOURED:
                this.submarineType = SUBMARINE_TYPE_ARMOURED;
                movementSpeedX = DEFAULT_MOVEMENT_SPEED_X * 0.9f;
                animationSpeed = 1f;
                texture = new Texture[]{assets.manager.get(com.cmy.abyssaldive.Assets.submarineArmored01), assets.manager.get(com.cmy.abyssaldive.Assets.submarineArmored02), assets.manager.get(com.cmy.abyssaldive.Assets.submarineArmored03), assets.manager.get(Assets.submarineArmored04)};
                lifePoints = DEFAULT_STARTING_LIFE + 2;
                batteryDrain = DEFAULT_BATTERY_DRAIN;
                break;
        }

    }

    public void update(float deltaTime, int movementDirection) {
        battery -= deltaTime * batteryDrain;
        if (battery < 0) battery = 0;


        if (state == STATE_INVULNERABLE) {
            timeLeftInvulnerability -= deltaTime;
            if (timeLeftInvulnerability < 0)
                state = STATE_ALIVE;
        }
        animate(deltaTime);
        move(deltaTime, movementDirection);
        submarineLight.update(movementDirection);
        submarineLight.setPosition(x, y);
    }

    public void animate(float deltaTime) {
        submarineAnimationState += deltaTime * animationSpeed;
        if (submarineAnimationState >= texture.length)
            submarineAnimationState = 0;
    }

    public void move(float deltaTime, int movementDirection) {
        x += movementDirection * movementSpeedX * deltaTime;
        if (x < 0)
            x = 0;
        else if (x > com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH - width)
            x = SubmarineGame.VIRTUAL_WIDTH - width;
        hitbox.x = x + (1 - hitboxScale) * width / 2;
        hitbox.y = y + (1 - hitboxScale) * height / 2;
    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.begin();
        spriteBatch.setColor(1f, 1f, 1f, 1f);
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.draw(getTexture(),
                x,
                y,
                width,
                height);
        spriteBatch.end();
    }

    public void reset() {
        state = STATE_ALIVE;
        x = DEFAULT_POS_X;
        y = DEFAULT_POS_Y;
        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;
        hitboxScale = DEFAULT_HITBOX_SCALE;
        submarineAnimationState = 0;
        setSubmarineType(submarineType);
        hitbox = new Rectangle(
                x + (1 - hitboxScale) * width / 2,
                y - height + (1 - hitboxScale) * height / 2,
                width * hitboxScale,
                height * hitboxScale
        );

        battery = 100f;
    }


    public void setPositionc(float x, float y) {
        this.x = x;
        this.y = y;
        hitbox.x = x + (1 - hitboxScale) * width / 2;
        hitbox.y = y + (1 - hitboxScale) * height / 2;
        submarineLight.setPosition(this.x, this.y);
    }

    public Texture getTexture() {
        if (state == STATE_INVULNERABLE) {
            if (((int) (timeLeftInvulnerability * 10)) % 4 >= 2) {
                return textureInvulnerable;
            }
        }
        return texture[(int) submarineAnimationState];
    }

    public int removeLife(int damage) {
        if (state != STATE_INVULNERABLE) {
            if (submarineType == SUBMARINE_TYPE_ARMOURED)
                lifePoints -= 1;
            else
                lifePoints -= damage;
            if (lifePoints <= 0)
                state = STATE_DEAD;
            else {
                state = STATE_INVULNERABLE;
                timeLeftInvulnerability = 2f; // 2 seconds invulnerable on hit
            }
        }

        return state;
    }

    public int increaseLife(int healthBoost) {
        if (lifePoints + healthBoost > 9)
            lifePoints = 9;
        else
            lifePoints += healthBoost;
        return lifePoints;
    }

    public void increaseBattery(float batteryCharge) {
        if (battery + batteryCharge > 100)
            battery = 100;
        else
            battery += batteryCharge;
    }
}
