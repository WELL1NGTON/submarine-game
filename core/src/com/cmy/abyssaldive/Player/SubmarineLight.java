package com.cmy.abyssaldive.Player;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cmy.abyssaldive.Assets;

public class SubmarineLight {
    public static final float DEFAULT_LIGHT_WIDTH = 1080 * 2;
    public static final float DEFAULT_LIGHT_HEIGHT = 1920 * 2;
    public static final float LIGHT_POINTING_MIDDLE = 0f;
    public static final float LIGHT_POINTING_LEFT = -15f;
    public static final float LIGHT_POINTING_RIGHT = 15f;
    public float x;
    public float y;
    public float width;
    public float height;
    public float rotation;
    public float submarineWidth;
    public float submarineHeight;
    public Texture ambientDarknessLevel;
    public Texture lanternLightStrengthTexture;
    private com.cmy.abyssaldive.Player.Submarine submarine;


    public SubmarineLight(com.cmy.abyssaldive.Assets assets, float x, float y, float submarineWidth, float submarineHeight, com.cmy.abyssaldive.Player.Submarine submarine) {
        ambientDarknessLevel = assets.manager.get(com.cmy.abyssaldive.Assets.submarineLantern);
        lanternLightStrengthTexture = assets.manager.get(Assets.submarineLanternStrength);
        this.submarine = submarine;
        this.x = x;
        this.y = y;
        this.submarineWidth = submarineWidth;
        this.submarineHeight = submarineHeight;
        width = DEFAULT_LIGHT_WIDTH;
        height = DEFAULT_LIGHT_HEIGHT;
        rotation = LIGHT_POINTING_MIDDLE;
    }

    public void update(int movementDirection) {
        switch (movementDirection) {
            case com.cmy.abyssaldive.Player.Submarine.SUBMARINE_STOPPED:
                rotation = LIGHT_POINTING_MIDDLE;
                break;
            case com.cmy.abyssaldive.Player.Submarine.MOVE_SUBMARINE_LEFT:
                rotation = LIGHT_POINTING_LEFT;
                break;
            case Submarine.MOVE_SUBMARINE_RIGHT:
                rotation = LIGHT_POINTING_RIGHT;
                break;
        }
    }

    public void setPosition(float posX, float posY) {
        this.x = posX;
        this.y = posY;
    }

    public void draw(SpriteBatch spriteBatch, float ambientLightLevel) {
        spriteBatch.begin();

        spriteBatch.setColor(1f, 1f, 1f, 1f - ambientLightLevel);
        spriteBatch.setBlendFunction(GL20.GL_SRC_COLOR, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.draw(
                ambientDarknessLevel,
                x - width / 2 + submarineWidth / 2,
                y - height * 0.75f + submarineHeight / 2,
                width / 2,
                height * 0.75f - submarineHeight / 2,
                width,
                height,
                1.5f,
                1.5f,
                rotation,
                0,
                0,
                ambientDarknessLevel.getWidth(),
                ambientDarknessLevel.getHeight(),
                false,
                false
        );

        float lanterStrength = submarine.battery / 100;
        if (ambientLightLevel > lanterStrength) {
            lanterStrength = ambientLightLevel;
        }

        spriteBatch.setColor(1f, 1f, 1f, 1f - lanterStrength);
        spriteBatch.setBlendFunction(GL20.GL_SRC_COLOR, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.draw(
                lanternLightStrengthTexture,
                x - width / 2 + submarineWidth / 2,
                y - height * 0.75f + submarineHeight / 2,
                width / 2,
                height * 0.75f - submarineHeight / 2,
                width,
                height,
                1.5f,
                1.5f,
                rotation,
                0,
                0,
                ambientDarknessLevel.getWidth(),
                ambientDarknessLevel.getHeight(),
                false,
                false
        );

        spriteBatch.end();
    }

    public void draw2test(SpriteBatch spriteBatch, float ambientLightLevel) {

    }

}
