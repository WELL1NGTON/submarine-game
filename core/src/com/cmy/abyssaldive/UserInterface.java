package com.cmy.abyssaldive;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.cmy.abyssaldive.Player.Submarine;

public class UserInterface {
    private final ShapeRenderer shapeRenderer;
    private final Assets assets;
    boolean debug = false;
    float padding = 5f;
    private Texture upperHud;
    private Texture lifeCounterTexture;
    private Texture submarineBatteryTexture;
    private BitmapFont gameHUDStatsBitmapFont;
    private float fontsScale;
    public Stage stage;
    private Button.ButtonStyle settingsButtonStyle;
    private Texture settingsIcon;
    private Button settingsButton;
    private Camera camera;
    private Viewport viewport;


    public UserInterface(Assets assets, Viewport viewport, Camera camera) {
        this.assets = assets;
        this.camera = camera;
        this.viewport = viewport;
        stage = new Stage(viewport);
        shapeRenderer = new ShapeRenderer();
//        Gdx.input.setInputProcessor(stage);
        lifeCounterTexture = assets.manager.get(Assets.submarine01);
        settingsIcon = assets.manager.get(Assets.settings);
        gameHUDStatsBitmapFont = assets.manager.get(Assets.regularsBitmapFont);
        submarineBatteryTexture = assets.manager.get(Assets.submarineBattery);

        gameHUDStatsBitmapFont.getData().setScale(1f);
        fontsScale = (90f) / (gameHUDStatsBitmapFont.getLineHeight()); //setting the height of the font to 90 pixels

//        configureSettingsButton();
//        configureMissileButton();
    }

    private void configureSettingsButton() {
        //Stats Area
        //SettingsIcon  (most right on screen)
        settingsButtonStyle = new Button.ButtonStyle();
        settingsButtonStyle.up = new TextureRegionDrawable(new TextureRegion(settingsIcon));
        settingsButtonStyle.down = new TextureRegionDrawable(new TextureRegion(settingsIcon));
        settingsButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(settingsIcon));
        //force the button to be 90x90. Obs.: texture is 141x155
        float settingsWidth = 90f;
        float settingsHeight = 90f;

        settingsButton = new Button(settingsButtonStyle);
        settingsButton.setPosition(
                SubmarineGame.VIRTUAL_WIDTH - (settingsWidth + padding),
                SubmarineGame.VIRTUAL_HEIGHT - (90 + padding)
        );
        settingsButton.setWidth(settingsWidth);
        settingsButton.setHeight(settingsHeight);

        stage.addActor(settingsButton);

        settingsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                Gdx.app.log("UserInterface", "settingsButton pressed");
            }
        });
    }



    public void setInputStageToThis() {
        Gdx.input.setInputProcessor(stage);
    }

    public void draw(SpriteBatch spriteBatch, Submarine submarine, float depthMeter) {

        spriteBatch.begin();
        if (debug) {
            //Stats (Life/counter/Battery) Area (very top, 1080x100)
            spriteBatch.draw(
                    upperHud,
                    0,
                    SubmarineGame.VIRTUAL_HEIGHT - 100,
                    SubmarineGame.VIRTUAL_WIDTH,
                    100
            );

            //Usables Area (under the stats area), 1080x125)
            spriteBatch.draw(
                    upperHud,
                    0,
                    SubmarineGame.VIRTUAL_HEIGHT - 100 - 125,
                    SubmarineGame.VIRTUAL_WIDTH,
                    125
            );

            //Buffs Area (under the usables area or under stats if there is no usable), 1080x50)
            spriteBatch.draw(
                    upperHud,
                    0,
                    SubmarineGame.VIRTUAL_HEIGHT - 100 - 125 - 50,
                    SubmarineGame.VIRTUAL_WIDTH,
                    50
            );
        }

        //Stats Area
        //Life Counter Texture (most left on screen)
        spriteBatch.draw(
                lifeCounterTexture,
                padding,
                SubmarineGame.VIRTUAL_HEIGHT * 0.95f + padding,
                128,
                64
        );

        //Stats Area
        //Life Counter Number (BitmapFont)
        gameHUDStatsBitmapFont.getData().setScale(fontsScale);
        gameHUDStatsBitmapFont.draw(
                spriteBatch,
                "x" + submarine.lifePoints,
                128 + padding * 2f,
                SubmarineGame.VIRTUAL_HEIGHT * 0.95f + 64 + padding
        );

        //Stats Area
        //Depth Meter Counter Number (BitmapFont) (middle screen -30). Obs.: 30 is an arbitrary number that felt good and centered
        gameHUDStatsBitmapFont.getData().setScale(fontsScale);
        gameHUDStatsBitmapFont.draw(
                spriteBatch,
                String.valueOf((int) depthMeter),
                SubmarineGame.VIRTUAL_WIDTH / 2f - 30,
                SubmarineGame.VIRTUAL_HEIGHT * 0.95f + 64 + padding
        );

        spriteBatch.end();

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        if(submarine.battery > 50)
            shapeRenderer.setColor(Color.FOREST);
        else if (submarine.battery > 15)
            shapeRenderer.setColor(Color.YELLOW);
        else
            shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(
                SubmarineGame.VIRTUAL_WIDTH - 100 - padding - 180 + 9,
                SubmarineGame.VIRTUAL_HEIGHT - 100 + 25 + padding,
                (submarineBatteryTexture.getWidth() - 18) * submarine.battery / 100,
                submarineBatteryTexture.getHeight() - 50
        );
        shapeRenderer.end();

        spriteBatch.begin();
        //Stats Area
        //Battery Texture (left of the settingsIcon)
        //obs.: texture is 90 is 180x90, but the "energy juice" will be drawn:
        //x: from the start of the texture +2, until the end of the texture -6
        //y: from the start of the texture +2, until the end of the texture -2
        spriteBatch.draw(
                submarineBatteryTexture,
                SubmarineGame.VIRTUAL_WIDTH - 100 - padding - 180,
                SubmarineGame.VIRTUAL_HEIGHT - 100 + padding,
                submarineBatteryTexture.getWidth(),
                submarineBatteryTexture.getHeight()
        );
        spriteBatch.end();

        stage.draw();
    }
}
