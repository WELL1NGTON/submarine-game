package com.cmy.abyssaldive;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Background {
    public static final int STATIC_BACKGROUND = 0;
    public static final int SCROLLING_UP_BACKGROUND = 1;
    public static final int TRANSITION_BACKGROUNDS_ABRUPT = 0;
    public static final int TRANSITION_BACKGROUNDS_SHOW_NEXT = 1;
    public static final float DEFAULT_ANIMATION_SPEED = 100;
    private Texture nextBackground;
    private Texture upperBackground;
    private Texture lowerBackground;
    private int animationMode;
    private float animationSpeed;
    private float posY;
    private Assets assets;

    public Background(Assets assets) {
        this.assets = assets;

        nextBackground = upperBackground = lowerBackground = assets.manager.get(Assets.menuBackground);
        animationMode = STATIC_BACKGROUND;
        posY = 0;
        animationSpeed = DEFAULT_ANIMATION_SPEED;
    }

    public void update(float deltaTime) {
        switch (animationMode) {
            case STATIC_BACKGROUND:
                posY = 0;
                upperBackground = lowerBackground = nextBackground;
                break;
            case SCROLLING_UP_BACKGROUND:
                posY += deltaTime * animationSpeed;
                if (posY > SubmarineGame.VIRTUAL_HEIGHT) {
                    posY = 0;
                    upperBackground = lowerBackground;
                    lowerBackground = nextBackground;
                }
                break;
        }
    }


    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.setColor(1f, 1f, 1f, 1f);
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.begin();
        spriteBatch.draw(upperBackground, 0, posY, SubmarineGame.VIRTUAL_WIDTH, SubmarineGame.VIRTUAL_HEIGHT);
        spriteBatch.draw(lowerBackground, 0, posY - lowerBackground.getHeight(), SubmarineGame.VIRTUAL_WIDTH, SubmarineGame.VIRTUAL_HEIGHT);
        spriteBatch.end();
    }

    public void setBackground(Texture background, int animationMode, int transitionType) {
        switch (transitionType) {
            case TRANSITION_BACKGROUNDS_ABRUPT:
                upperBackground = background;
                lowerBackground = background;
            case TRANSITION_BACKGROUNDS_SHOW_NEXT:
                nextBackground = background;
                break;
        }
        this.animationMode = animationMode;
    }
}
