package com.cmy.abyssaldive.Menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.Background;
import com.cmy.abyssaldive.SubmarineGame;

import java.util.Locale;

public class InstructionsMenu {
    public Stage stage;
    TextButton goBackButton;
    Preferences preferences;
    Locale locale;
    Texture background;
    private I18NBundle languageStrings;
    private BitmapFont menuOptionsFont;

    public InstructionsMenu(final com.cmy.abyssaldive.Assets assets, I18NBundle languageStrings, Viewport viewport, final SubmarineGame submarineGame, Preferences preferences) {
        this.languageStrings = languageStrings;
        this.preferences = preferences;
        stage = new Stage(viewport);

        locale = new Locale(preferences.getString("language"), preferences.getString("country"));

        if (preferences.getString("language").equals("pt"))
            background = assets.manager.get(com.cmy.abyssaldive.Assets.instructionsBackgroundPt);
        else
            background = assets.manager.get(com.cmy.abyssaldive.Assets.instructionsBackgroundEn);

        menuOptionsFont = assets.manager.get(com.cmy.abyssaldive.Assets.regularsBitmapFont);

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        menuOptionsFont.getData().setScale(1.5f);
        textButtonStyle.font = menuOptionsFont;
        textButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonUp)));
        textButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonDown)));
        textButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonChecked)));

        goBackButton = new TextButton(languageStrings.get("go_back"), textButtonStyle);
        goBackButton.setPosition(25, 200);
        goBackButton.setWidth(225);
        goBackButton.setHeight(115);
        goBackButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                submarineGame.gameState = SubmarineGame.GAME_STATE_MAIN_MENU;
                submarineGame.background.setBackground(assets.manager.get(Assets.menuBackground), Background.STATIC_BACKGROUND, Background.TRANSITION_BACKGROUNDS_ABRUPT);
                submarineGame.mainMenu.setInputStageToThis();
                goBackButton.setChecked(false);
                submarineGame.goToMainMenu();
            }
        });

        stage.addActor(goBackButton);



    }

    public void setInputStageToThis() {
        Gdx.input.setInputProcessor(stage);
    }

    public void update(float deltaTime) {

    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.begin();
        spriteBatch.draw(background, 0, 0);
        spriteBatch.end();
        menuOptionsFont.getData().setScale(1f);
        stage.draw();
        stage.act();
    }


}
