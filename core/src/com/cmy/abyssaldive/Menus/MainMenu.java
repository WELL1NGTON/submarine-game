package com.cmy.abyssaldive.Menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.SubmarineGame;
import com.cmy.abyssaldive.Background;

public class MainMenu {
    public static final float DEFAULT_ANIMATION_SPEED = 0.5f;
    public static final int TEXT_GROWING = 1;
    public static final int TEXT_SHRINKING = -1;
    private float audioVolume;
    public Button settingsButton;
    public Button instructionsButton;
    public TextButton startGameButton;
    public Stage stage;
    private float animationSpeed;
    private BitmapFont menuOptionsFont;
    private float menuOptionsScale;
    private float startGameScaleMax;
    private float startGameScaleMin;
    private int startGameScaleVariatonDirection;
    private float startGameWidth;
    private float startGameHeight;
    private float instructionsWidth;
    private float instructionsHeight;
    private float settingsWidth;
    private float settingsHeight;
    private Sound musicMenu;
    private long musicMenuId;


    public MainMenu(final com.cmy.abyssaldive.Assets assets, I18NBundle languageStrings, Viewport viewport, final com.cmy.abyssaldive.SubmarineGame submarineGame, float audioVolume) {
        stage = new Stage(viewport);
        this.audioVolume = audioVolume;
        menuOptionsFont = assets.manager.get(com.cmy.abyssaldive.Assets.regularsBitmapFont);
        musicMenu = assets.manager.get(com.cmy.abyssaldive.Assets.underwaterSound);
        Texture settingsIcon = assets.manager.get(com.cmy.abyssaldive.Assets.settings);
        Texture instructionsIcon = assets.manager.get(com.cmy.abyssaldive.Assets.instructions);

        GlyphLayout layout = new GlyphLayout();

        animationSpeed = DEFAULT_ANIMATION_SPEED;

        menuOptionsScale = 1f;
        startGameScaleVariatonDirection = TEXT_GROWING;
        startGameScaleMax = menuOptionsScale + 0.1f;
        startGameScaleMin = menuOptionsScale - 0.1f;


        String startGameString = languageStrings.get("menu_start_game");
        layout.setText(menuOptionsFont, startGameString);
        startGameWidth = layout.width;
        startGameHeight = layout.height;

        musicMenuId = -1;

        TextButton.TextButtonStyle startGameButtonStyle = new TextButton.TextButtonStyle();
        menuOptionsFont.getData().setScale(1.5f * menuOptionsScale);
        startGameButtonStyle.font = menuOptionsFont;
        startGameButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonUp)));
        startGameButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonDown)));
        startGameButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonChecked)));

        startGameWidth = 700;
        startGameHeight = 200;
        startGameButton = new TextButton(languageStrings.get("menu_start_game"), startGameButtonStyle);
        startGameButton.setPosition(com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH / 2f - startGameWidth * menuOptionsScale / 2, com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.7f - startGameHeight * menuOptionsScale / 2);
        startGameButton.setWidth(startGameWidth);
        startGameButton.setHeight(startGameHeight);
        startGameButton.getStyle().checkedFontColor = new Color(1f, 1f, 1f, 1f);
        startGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                stopMenuMusic();
                submarineGame.resetGame();
                submarineGame.gameState = com.cmy.abyssaldive.SubmarineGame.GAME_STATE_RUNNING;
                submarineGame.background.setBackground(assets.manager.get(com.cmy.abyssaldive.Assets.oceanBackground01), Background.SCROLLING_UP_BACKGROUND, Background.TRANSITION_BACKGROUNDS_ABRUPT);
                submarineGame.userInterface.setInputStageToThis();
                submarineGame.gameMusic.loop(submarineGame.audioVolume);
            }
        });

        Button.ButtonStyle settingsButtonStyle = new Button.ButtonStyle();
        settingsButtonStyle.up = new TextureRegionDrawable(new TextureRegion(settingsIcon));
        settingsButtonStyle.down = new TextureRegionDrawable(new TextureRegion(settingsIcon));
        settingsButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(settingsIcon));

        settingsWidth = 141f;
        settingsHeight = 155f;
        settingsButton = new Button(settingsButtonStyle);
        settingsButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.3f - settingsWidth * menuOptionsScale / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.4f - settingsHeight * menuOptionsScale / 2
        );
        settingsButton.setWidth(settingsWidth * menuOptionsScale);
        settingsButton.setHeight(settingsHeight * menuOptionsScale);
        settingsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
//                submarineGame.resetGame();
                submarineGame.gameState = com.cmy.abyssaldive.SubmarineGame.GAME_STATE_SETTINGS_MENU;
                submarineGame.background.setBackground(assets.manager.get(com.cmy.abyssaldive.Assets.menuBackground), Background.STATIC_BACKGROUND, Background.TRANSITION_BACKGROUNDS_ABRUPT);
                submarineGame.settingsMenu.setInputStageToThis();
            }
        });

        Button.ButtonStyle instructionsButtonStyle = new Button.ButtonStyle();
        instructionsButtonStyle.up = new TextureRegionDrawable(new TextureRegion(instructionsIcon));
        instructionsButtonStyle.down = new TextureRegionDrawable(new TextureRegion(instructionsIcon));
        instructionsButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(instructionsIcon));

        instructionsWidth = 60f;
        instructionsHeight = 152f;
        instructionsButton = new Button(instructionsButtonStyle);
        instructionsButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.7f - instructionsWidth * menuOptionsScale / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.4f - instructionsHeight * menuOptionsScale / 2
        );
        instructionsButton.setWidth(instructionsWidth * menuOptionsScale);
        instructionsButton.setHeight(instructionsHeight * menuOptionsScale);
        instructionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
//                submarineGame.resetGame();
                submarineGame.gameState = com.cmy.abyssaldive.SubmarineGame.GAME_STATE_INSTRUCTIONS_MENU;
                submarineGame.background.setBackground(assets.manager.get(Assets.menuBackground), Background.STATIC_BACKGROUND, Background.TRANSITION_BACKGROUNDS_ABRUPT);
                submarineGame.instructionsMenu.setInputStageToThis();
            }
        });

        stage.addActor(startGameButton);
        stage.addActor(settingsButton);
        stage.addActor(instructionsButton);
    }

    public void setInputStageToThis() {
        Gdx.input.setInputProcessor(stage);
    }

    public void update(float deltaTime) {

        menuOptionsScale += deltaTime / 4 * animationSpeed * startGameScaleVariatonDirection;

        menuOptionsFont.getData().setScale(1.5f * menuOptionsScale);

        startGameButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH / 2f - startGameWidth * menuOptionsScale / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.7f - startGameHeight * menuOptionsScale / 2
        );
        startGameButton.setWidth(startGameWidth * menuOptionsScale);
        startGameButton.setHeight(startGameHeight * menuOptionsScale);

        settingsButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.3f - settingsWidth * menuOptionsScale / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.4f - settingsHeight * menuOptionsScale / 2
        );
        settingsButton.setWidth(settingsWidth * menuOptionsScale);
        settingsButton.setHeight(settingsHeight * menuOptionsScale);

        instructionsButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.7f - instructionsWidth * menuOptionsScale / 2,
                SubmarineGame.VIRTUAL_HEIGHT * 0.4f - instructionsHeight * menuOptionsScale / 2
        );
        instructionsButton.setWidth(instructionsWidth * menuOptionsScale);
        instructionsButton.setHeight(instructionsHeight * menuOptionsScale);

        if (menuOptionsScale > startGameScaleMax) {
            startGameScaleVariatonDirection = TEXT_SHRINKING;
        } else if (menuOptionsScale < startGameScaleMin) {
            startGameScaleVariatonDirection = TEXT_GROWING;
        }
    }

    public void draw() {
        stage.draw();
    }

    public void playMenuMusic() {
        if (musicMenuId == -1)
            musicMenuId = musicMenu.loop(audioVolume);
    }

    public void stopMenuMusic() {
        if (musicMenuId != -1) {
            musicMenu.stop(musicMenuId);
            musicMenuId = -1;
        }
    }

    public void changeVolume(float audioVolume) {
        this.audioVolume = audioVolume;

        if (musicMenuId != -1)
            musicMenu.setVolume(musicMenuId, audioVolume);
    }
}
