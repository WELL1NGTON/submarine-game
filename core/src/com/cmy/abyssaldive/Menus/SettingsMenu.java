package com.cmy.abyssaldive.Menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.SubmarineGame;
import com.cmy.abyssaldive.Background;
import com.cmy.abyssaldive.Player.Submarine;

import java.util.Locale;

public class SettingsMenu {
    private final com.cmy.abyssaldive.SubmarineGame submarineGame;
    public Stage stage;
    Button ptBrButton;
    Button enUsButton;
    TextButton defaultSubmarineButton;
    TextButton nuclearSubmarineButton;
    TextButton armouredSubmarineButton;
    TextButton applyButton;
    TextButton goBackButton;
    Preferences preferences;
    Locale locale;
    Texture defaultSubmarine;
    Texture nuclearSubmarine;
    Slider.SliderStyle gameVolumeSliderStyle;
    Slider gameVolumeSlider;
    Container<Slider> gameVolumeSliderContainer;
    int submarineType;
    private I18NBundle languageStrings;
    private BitmapFont menuOptionsFont;


    public SettingsMenu(final com.cmy.abyssaldive.Assets assets, I18NBundle languageStrings, Viewport viewport, final com.cmy.abyssaldive.SubmarineGame submarineGame, Preferences preferences) {
        this.languageStrings = languageStrings;
        this.preferences = preferences;
        this.submarineGame = submarineGame;
        stage = new Stage(viewport);

        submarineType = submarineGame.submarineType;


        locale = new Locale(preferences.getString("language"), preferences.getString("country"));

        defaultSubmarine = assets.manager.get(com.cmy.abyssaldive.Assets.submarine01);
        nuclearSubmarine = assets.manager.get(com.cmy.abyssaldive.Assets.submarine01);

        menuOptionsFont = assets.manager.get(com.cmy.abyssaldive.Assets.regularsBitmapFont);

        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        menuOptionsFont.getData().setScale(1.5f);
        textButtonStyle.font = menuOptionsFont;
        textButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonUp)));
        textButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonDown)));
        textButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.buttonChecked)));

        setPtBrButton(assets);
        setEnUsButton(assets);
        setGoBackButton(assets, textButtonStyle);
        setApplyButton(assets, textButtonStyle);
        setSlider(assets);
        setDefaultSubmarineButton(assets);
        setNuclearSubmarineButton(assets);
        setArmouredSubmarineButton(assets);
        if (submarineType == Submarine.SUBMARINE_TYPE_NUCLEAR)
            nuclearSubmarineButton.setChecked(true);
        else if (submarineType == Submarine.SUBMARINE_TYPE_ARMOURED)
            armouredSubmarineButton.setChecked(true);
        else
            defaultSubmarineButton.setChecked(true);

//        stage.addActor(textField);
        stage.addActor(gameVolumeSliderContainer);
        stage.addActor(goBackButton);
        stage.addActor(applyButton);
        stage.addActor(ptBrButton);
        stage.addActor(enUsButton);
        stage.addActor(defaultSubmarineButton);
        stage.addActor(nuclearSubmarineButton);
        stage.addActor(armouredSubmarineButton);

        if (locale.getLanguage().equals("pt"))
            ptBrButton.setChecked(true);
        else
            enUsButton.setChecked(true);
    }

    private void setSlider(com.cmy.abyssaldive.Assets assets) {
        gameVolumeSliderStyle = new Slider.SliderStyle(
                new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.sliderBackground01))),
                new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.sliderKnob01)))
        );

        gameVolumeSlider = new Slider(0, 100, 1, false, gameVolumeSliderStyle);
        gameVolumeSlider.setValue(submarineGame.audioVolume * 100);

        gameVolumeSlider.addCaptureListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                submarineGame.mainMenu.changeVolume(gameVolumeSlider.getValue() / 100f);
            }
        });

        gameVolumeSliderContainer = new Container<>(gameVolumeSlider);
        gameVolumeSliderContainer.setTransform(true);
        gameVolumeSliderContainer.setSize(300, 50);
        gameVolumeSliderContainer.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH / 2 - 300 * 3 / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.2f
        );
        gameVolumeSliderContainer.setScale(3);
    }

    private void setApplyButton(final com.cmy.abyssaldive.Assets assets, TextButton.TextButtonStyle textButtonStyle) {
        applyButton = new TextButton(languageStrings.get("apply"), textButtonStyle);
        applyButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH / 2 - 400,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.125f
        );
        applyButton.setWidth(350);
        applyButton.setHeight(150);
        applyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                submarineGame.gameState = com.cmy.abyssaldive.SubmarineGame.GAME_STATE_MAIN_MENU;
                submarineGame.background.setBackground(assets.manager.get(com.cmy.abyssaldive.Assets.menuBackground), Background.STATIC_BACKGROUND, Background.TRANSITION_BACKGROUNDS_ABRUPT);
                float volume = gameVolumeSlider.getValue();
                Gdx.app.log("volume", "" + volume);
                applyButton.setChecked(false);

                submarineGame.audioVolume = gameVolumeSlider.getValue() / 100f;
                submarineGame.submarineType = submarineType;

                SettingsMenu.this.preferences = Gdx.app.getPreferences("gamePreferences");
                SettingsMenu.this.preferences.putString("country", locale.getCountry());
                SettingsMenu.this.preferences.putString("language", locale.getLanguage());
                SettingsMenu.this.preferences.putFloat("audioVolume", submarineGame.audioVolume);
                SettingsMenu.this.preferences.putInteger("submarineType", submarineType);
                SettingsMenu.this.preferences.flush();

                submarineGame.resetGame();
                submarineGame.mainMenu.setInputStageToThis();
                submarineGame.goToMainMenu();
            }
        });
    }

    private void setGoBackButton(final com.cmy.abyssaldive.Assets assets, TextButton.TextButtonStyle textButtonStyle) {
        goBackButton = new TextButton(languageStrings.get("go_back"), textButtonStyle);
        goBackButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH / 2 + 50,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.125f
        );
        goBackButton.setWidth(350);
        goBackButton.setHeight(150);
        goBackButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                submarineGame.gameState = com.cmy.abyssaldive.SubmarineGame.GAME_STATE_MAIN_MENU;
                submarineGame.background.setBackground(assets.manager.get(com.cmy.abyssaldive.Assets.menuBackground), Background.STATIC_BACKGROUND, Background.TRANSITION_BACKGROUNDS_ABRUPT);
                submarineGame.mainMenu.setInputStageToThis();
                goBackButton.setChecked(false);
                submarineGame.goToMainMenu();
            }
        });
    }

    private void setEnUsButton(com.cmy.abyssaldive.Assets assets) {
        Button.ButtonStyle enUsButtonStyle = new Button.ButtonStyle();
        enUsButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.languageSelectEnUsIconChecked)));
        enUsButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.languageSelectEnUsIconChecked)));
        enUsButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.languageSelectEnUsIcon)));

        enUsButton = new Button(enUsButtonStyle);
        enUsButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.75f - 256 / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.7f - 256 / 2
        );
        enUsButton.setSize(256, 256);
        enUsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                enUsButton.setChecked(true);
                ptBrButton.setChecked(false);

                locale = new Locale("en", "US");
                SettingsMenu.this.languageStrings = I18NBundle.createBundle(Gdx.files.internal("strings/strings"), locale);
                goBackButton.setText(SettingsMenu.this.languageStrings.get("go_back"));
                applyButton.setText(SettingsMenu.this.languageStrings.get("apply"));
            }
        });
    }

    private void setPtBrButton(com.cmy.abyssaldive.Assets assets) {
        Button.ButtonStyle ptBrButtonStyle = new Button.ButtonStyle();
        ptBrButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.languageSelectPtBrIconChecked)));
        ptBrButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.languageSelectPtBrIconChecked)));
        ptBrButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.languageSelectPtBrIcon)));

        ptBrButton = new Button(ptBrButtonStyle);
        ptBrButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.25f - 256 / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.7f - 256 / 2
        );
        ptBrButton.setSize(256, 256);
        ptBrButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                ptBrButton.setChecked(true);
                enUsButton.setChecked(false);

                locale = new Locale("pt", "BR");
                SettingsMenu.this.languageStrings = I18NBundle.createBundle(Gdx.files.internal("strings/strings"), locale);
                goBackButton.setText(SettingsMenu.this.languageStrings.get("go_back"));
                applyButton.setText(SettingsMenu.this.languageStrings.get("apply"));
            }
        });
    }

    private void setDefaultSubmarineButton(com.cmy.abyssaldive.Assets assets) {
        TextButton.TextButtonStyle defaultSubmarineButtonStyle = new TextButton.TextButtonStyle();
        defaultSubmarineButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarineDefaultChecked)));
        defaultSubmarineButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarineDefaultChecked)));
        defaultSubmarineButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarine01)));
        defaultSubmarineButtonStyle.font = menuOptionsFont;

        defaultSubmarineButton = new TextButton("", defaultSubmarineButtonStyle);
        defaultSubmarineButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.2f - 256 / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.5f - 256 / 2
        );
        defaultSubmarineButton.setSize(256, 128);
        defaultSubmarineButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                defaultSubmarineButton.setChecked(true);
                nuclearSubmarineButton.setChecked(false);
                armouredSubmarineButton.setChecked(false);
                submarineType = Submarine.SUBMARINE_TYPE_DEFAULT;
            }
        });
    }

    private void setNuclearSubmarineButton(com.cmy.abyssaldive.Assets assets) {
        TextButton.TextButtonStyle nuclearSubmarineButtonStyle = new TextButton.TextButtonStyle();
        nuclearSubmarineButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarineNuclearChecked)));
        nuclearSubmarineButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarineNuclearChecked)));
        nuclearSubmarineButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarineNuclear01)));
        nuclearSubmarineButtonStyle.font = new BitmapFont(Gdx.files.internal("regulars.fnt"));
//        nuclearSubmarineButtonStyle.font = assets.manager.get(Assets.regularsBitmapFont).newFontCache().getFont();

        float requiredScore = 70000;

        nuclearSubmarineButton = new TextButton("", nuclearSubmarineButtonStyle);
        nuclearSubmarineButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.50f - 256 / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.5f - 256 / 2
        );
        nuclearSubmarineButton.setSize(256, 128);
        if (submarineGame.totalScore < requiredScore) {
            nuclearSubmarineButtonStyle.font.getData().setScale(0.4f);
            nuclearSubmarineButton.setText((int) requiredScore + " totalScore");

            nuclearSubmarineButton.setColor(0, 0, 0, 1);
        } else {
            nuclearSubmarineButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    defaultSubmarineButton.setChecked(false);
                    nuclearSubmarineButton.setChecked(true);
                    armouredSubmarineButton.setChecked(false);
                    submarineType = Submarine.SUBMARINE_TYPE_NUCLEAR;
                }
            });
        }
    }

    private void setArmouredSubmarineButton(com.cmy.abyssaldive.Assets assets) {
        TextButton.TextButtonStyle armouredSubmarineButtonStyle = new TextButton.TextButtonStyle();
        armouredSubmarineButtonStyle.up = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarineArmoredChecked)));
        armouredSubmarineButtonStyle.down = new TextureRegionDrawable(new TextureRegion(assets.manager.get(com.cmy.abyssaldive.Assets.submarineArmoredChecked)));
        armouredSubmarineButtonStyle.checked = new TextureRegionDrawable(new TextureRegion(assets.manager.get(Assets.submarineArmored01)));
        armouredSubmarineButtonStyle.font = new BitmapFont(Gdx.files.internal("regulars.fnt"));
//        nuclearSubmarineButtonStyle.font = assets.manager.get(Assets.regularsBitmapFont).newFontCache().getFont();

        float requiredScore = 700;

        armouredSubmarineButton = new TextButton("", armouredSubmarineButtonStyle);
        armouredSubmarineButton.setPosition(
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.8f - 256 / 2,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.5f - 256 / 2
        );
        armouredSubmarineButton.setSize(256, 128);
        if (submarineGame.highScore < requiredScore) {
            armouredSubmarineButtonStyle.font.getData().setScale(0.4f);
            armouredSubmarineButton.setText((int) requiredScore + " highScore");

            armouredSubmarineButton.setColor(0, 0, 0, 1);
        } else {
            armouredSubmarineButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    defaultSubmarineButton.setChecked(false);
                    nuclearSubmarineButton.setChecked(false);
                    armouredSubmarineButton.setChecked(true);
                    submarineType = Submarine.SUBMARINE_TYPE_ARMOURED;
                }
            });
        }
    }

    public void setInputStageToThis() {
        Gdx.input.setInputProcessor(stage);
    }

    public void update(float deltaTime) {

    }

    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.begin();
        menuOptionsFont.getData().setScale(1.5f);

        spriteBatch.setColor(1f, 1f, 1f, 1f);
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        menuOptionsFont.draw(
                spriteBatch,
                languageStrings.get("select_language"),
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.5f - 500,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.85f
        );
        menuOptionsFont.draw(
                spriteBatch,
                "Audio Volume:",
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_WIDTH * 0.5f - 250,
                SubmarineGame.VIRTUAL_HEIGHT * 0.35f
        );


        spriteBatch.end();
        stage.draw();
        stage.act();
    }


}
