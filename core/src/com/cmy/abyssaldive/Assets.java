package com.cmy.abyssaldive;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Assets {
    //Loading Screen Texture
    public static final AssetDescriptor<Texture> loadingScreen = new AssetDescriptor<>("loading_screen.png", Texture.class);
    //Submarine Textures
    public static final AssetDescriptor<Texture> submarine01 = new AssetDescriptor<>("submarine1.png", Texture.class);
    public static final AssetDescriptor<Texture> submarine02 = new AssetDescriptor<>("submarine2.png", Texture.class);
    public static final AssetDescriptor<Texture> submarine03 = new AssetDescriptor<>("submarine3.png", Texture.class);
    public static final AssetDescriptor<Texture> submarine04 = new AssetDescriptor<>("submarine4.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineDefaultChecked = new AssetDescriptor<>("submarine_default_checked.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineNuclear01 = new AssetDescriptor<>("submarine_nuclear1.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineNuclear02 = new AssetDescriptor<>("submarine_nuclear2.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineNuclear03 = new AssetDescriptor<>("submarine_nuclear3.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineNuclear04 = new AssetDescriptor<>("submarine_nuclear4.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineNuclearChecked = new AssetDescriptor<>("submarine_nuclear_checked.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineArmored01 = new AssetDescriptor<>("submarine_armored1.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineArmored02 = new AssetDescriptor<>("submarine_armored2.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineArmored03 = new AssetDescriptor<>("submarine_armored3.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineArmored04 = new AssetDescriptor<>("submarine_armored4.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineArmoredChecked = new AssetDescriptor<>("submarine_armored_checked.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineInvulnerable01 = new AssetDescriptor<>("submarine_invulnerable01.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineInvulnerable02 = new AssetDescriptor<>("submarine_invulnerable02.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineLantern = new AssetDescriptor<>("submarine_lantern.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineLanternStrength = new AssetDescriptor<>("submarine_lantern_strength.png", Texture.class);
    public static final AssetDescriptor<Texture> submarineBattery = new AssetDescriptor<>("submarine_battery.png", Texture.class);
    //Rock textures
    public static final AssetDescriptor<Texture> rock01 = new AssetDescriptor<>("rock_newmodel.png", Texture.class);
    public static final AssetDescriptor<Texture> rock01Broken01 = new AssetDescriptor<>("rock_broken1.png", Texture.class);
    public static final AssetDescriptor<Texture> rock01Broken02 = new AssetDescriptor<>("rock_broken2.png", Texture.class);
    public static final AssetDescriptor<Texture> rock01Broken03 = new AssetDescriptor<>("rock_broken3.png", Texture.class);
    //Naval Mine
    public static final AssetDescriptor<Texture> navalMine = new AssetDescriptor<>("naval_mine.png", Texture.class);
    public static final AssetDescriptor<Texture> navalMineExplosion01 = new AssetDescriptor<>("naval_mine_explosion_01.png", Texture.class);
    public static final AssetDescriptor<Texture> navalMineExplosion02 = new AssetDescriptor<>("naval_mine_explosion_02.png", Texture.class);
    public static final AssetDescriptor<Texture> navalMineExplosion03 = new AssetDescriptor<>("naval_mine_explosion_03.png", Texture.class);
    //Backgrounds Textures
    public static final AssetDescriptor<Texture> oceanBackground01 = new AssetDescriptor<>("ocean_background.png", Texture.class);
    public static final AssetDescriptor<Texture> menuBackground = new AssetDescriptor<>("menu_background.png", Texture.class);
    public static final AssetDescriptor<Texture> instructionsBackgroundEn = new AssetDescriptor<>("instructions_background_en.png", Texture.class);
    public static final AssetDescriptor<Texture> instructionsBackgroundPt = new AssetDescriptor<>("instructions_background_pt.png", Texture.class);
    //Sounds
    public static final AssetDescriptor<Sound> rockCollisionSound = new AssetDescriptor<>("rock_collision.wav", Sound.class);
    public static final AssetDescriptor<Sound> mineExplosionSound = new AssetDescriptor<>("mine_explosion.wav", Sound.class);
    public static final AssetDescriptor<Sound> lowerBattery = new AssetDescriptor<>("lower_battery.wav", Sound.class);
    public static final AssetDescriptor<Sound> powerBattery = new AssetDescriptor<>("power_battery.wav", Sound.class);
    public static final AssetDescriptor<Sound> underwaterSound = new AssetDescriptor<>("underwater_sound.wav", Sound.class);
    //Fonts
    public static final AssetDescriptor<BitmapFont> regularsBitmapFont = new AssetDescriptor<>(Gdx.files.internal("regulars.fnt"), BitmapFont.class);
    //UserInterface
    public static final AssetDescriptor<Texture> settings = new AssetDescriptor<>("settings.png", Texture.class);
    public static final AssetDescriptor<Texture> instructions = new AssetDescriptor<>("instructions.png", Texture.class);
    public static final AssetDescriptor<Texture> buttonUp = new AssetDescriptor<>("button_up.png", Texture.class);
    public static final AssetDescriptor<Texture> buttonDown = new AssetDescriptor<>("button_down.png", Texture.class);
    public static final AssetDescriptor<Texture> buttonChecked = new AssetDescriptor<>("button_checked.png", Texture.class);
    public static final AssetDescriptor<Texture> sliderBackground01 = new AssetDescriptor<>("slider_background2.png", Texture.class);
    public static final AssetDescriptor<Texture> sliderKnob01 = new AssetDescriptor<>("slider_knob2.png", Texture.class);
    public static final AssetDescriptor<Texture> languageSelectEnUsIcon = new AssetDescriptor<>("language_select_en_us_icon.png", Texture.class);
    public static final AssetDescriptor<Texture> languageSelectEnUsIconChecked = new AssetDescriptor<>("language_select_en_us_icon_checked.png", Texture.class);
    public static final AssetDescriptor<Texture> languageSelectPtBrIcon = new AssetDescriptor<>("language_select_pt_br_icon.png", Texture.class);
    public static final AssetDescriptor<Texture> languageSelectPtBrIconChecked = new AssetDescriptor<>("language_select_pt_br_icon_checked.png", Texture.class);
    //Collectibles
    public static final AssetDescriptor<Texture> nuclearBattery1 = new AssetDescriptor<>("nuclear_batery1.png", Texture.class);
    public static final AssetDescriptor<Texture> nuclearBattery2 = new AssetDescriptor<>("nuclear_batery2.png", Texture.class);
    public static final AssetDescriptor<Texture> lowerBattery1 = new AssetDescriptor<>("lower_batery1.png", Texture.class);
    public static final AssetDescriptor<Texture> lowerBattery2 = new AssetDescriptor<>("lower_batery2.png", Texture.class);


    public final AssetManager manager = new AssetManager();


    public void loadLoadingScreen() {
        manager.load(loadingScreen);
    }

    public void loadSubmarineTextures() {
        manager.load(submarine01);
        manager.load(submarine02);
        manager.load(submarine03);
        manager.load(submarine04);
        manager.load(submarineNuclear01);
        manager.load(submarineNuclear02);
        manager.load(submarineNuclear03);
        manager.load(submarineNuclear04);
        manager.load(submarineArmored01);
        manager.load(submarineArmored02);
        manager.load(submarineArmored03);
        manager.load(submarineArmored04);
        manager.load(submarineInvulnerable01);
        manager.load(submarineInvulnerable02);
        manager.load(submarineLantern);
        manager.load(submarineLanternStrength);
        manager.load(submarineBattery);
    }

    public void loadEnemies() {
        manager.load(rock01);
        manager.load(rock01Broken01);
        manager.load(rock01Broken02);
        manager.load(rock01Broken03);

        manager.load(navalMine);
        manager.load(navalMineExplosion01);
        manager.load(navalMineExplosion02);
        manager.load(navalMineExplosion03);
    }

    public void loadBackgrounds() {
        manager.load(oceanBackground01);
        manager.load(menuBackground);
        manager.load(instructionsBackgroundPt);
        manager.load(instructionsBackgroundEn);
    }

    public void loadSounds() {
        manager.load(rockCollisionSound);
        manager.load(mineExplosionSound);
        manager.load(underwaterSound);
        manager.load(lowerBattery);
        manager.load(powerBattery);
    }

    public void loadFonts() {
        manager.load(regularsBitmapFont);
    }

    public void loadUserInterface() {
        manager.load(settings);
        manager.load(instructions);
        manager.load(buttonUp);
        manager.load(buttonDown);
        manager.load(buttonChecked);
        manager.load(sliderBackground01);
        manager.load(sliderKnob01);
        manager.load(languageSelectEnUsIcon);
        manager.load(languageSelectEnUsIconChecked);
        manager.load(languageSelectPtBrIcon);
        manager.load(languageSelectPtBrIconChecked);
        manager.load(submarineDefaultChecked);
        manager.load(submarineNuclearChecked);
        manager.load(submarineArmoredChecked);
    }

    public void loadCollectibles() {
        manager.load(nuclearBattery1);
        manager.load(nuclearBattery2);
        manager.load(lowerBattery1);
        manager.load(lowerBattery2);
    }
}
