package com.cmy.abyssaldive.Collectables;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.Player.Submarine;
import com.cmy.abyssaldive.SubmarineGame;

public abstract class Collectible {
    public static final int ALIVE = 0x01; //0000 0001
    public static final int CAN_INTERACT_WITH_PLAYER = 0x02; //0000 0010
    public static final int VISIBLE = 0x04; //0000 0100
    public static final int MOVING = 0x08; //0000 1000
    public int objectState;
    public float x;
    public float y;
    public float width;
    public float height;
    public Circle hitbox;
    public com.cmy.abyssaldive.Assets assets;
    public Texture[] textures;
    public float hitboxScale;
    public float movementSpeedY;
    public float animationState;
    public float finalAnimationState;
    public float animationSpeed;
    public float rotation;
    public float rotationSpeed;
    com.cmy.abyssaldive.SubmarineGame submarineGame;

    public Collectible(Assets assets, com.cmy.abyssaldive.SubmarineGame submarineGame) {
        this.assets = assets;
        this.submarineGame = submarineGame;


        width = 200f;
        height = 200f;
        x = 560;
        y = -height;

        animationSpeed = 15f;
        animationState = 0;
        movementSpeedY = 600f;
        hitboxScale = 0.95f;
        rotation = 0f;
        rotationSpeed = 0f;

        hitbox = new Circle(
                x + width / 2,
                y + height / 2,
                hitboxScale * width / 2
        );

        finalAnimationState = 0;

        objectState = ALIVE | CAN_INTERACT_WITH_PLAYER | VISIBLE | MOVING;
    }

    public void update(float deltaTime) {
        if (isMoving()) {
            animationState += deltaTime * animationSpeed;
            if (animationState >= textures.length) animationState = 0;
            rotation += deltaTime * rotationSpeed;
            if (rotation >= 360) rotation = 0;

            y += deltaTime * movementSpeedY;
            hitbox.setY(y + height / 2);

            if (y > SubmarineGame.VIRTUAL_HEIGHT + height / 2)
                removeState(ALIVE);
        }
    }

    public boolean isTouchingPlayer(com.cmy.abyssaldive.Player.Submarine submarine) {
        if (canInteractWithPlayer())
            return Intersector.overlaps(this.hitbox, submarine.hitbox);
        return false;
    }

    public boolean isAlive() {
        return (objectState & ALIVE) != 0;
    }

    public boolean isMoving() {
        return (objectState & MOVING) != 0;
    }

    public boolean canInteractWithPlayer() {
        return (objectState & CAN_INTERACT_WITH_PLAYER) != 0;
    }

    public boolean isVisible() {
        return (objectState & VISIBLE) != 0;
    }

    public void applyEffectToPlayer(Submarine submarine) {
        //no effect on the example
    }

    public void draw(SpriteBatch spriteBatch) {
        if (isVisible()) {
            spriteBatch.begin();
            spriteBatch.setColor(1f, 1f, 1f, 1f);
            spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            spriteBatch.draw(
                    getTexture(),
                    x,
                    y,
                    width / 2,
                    height / 2,
                    width,
                    height,
                    1,
                    1,
                    rotation,
                    0,
                    0,
                    getTexture().getWidth(),
                    getTexture().getHeight(),
                    false,
                    false
            );
            spriteBatch.end();
        }
    }

    public void drawHitbox(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.OLIVE);
        shapeRenderer.circle(
                hitbox.x,
                hitbox.y,
                hitbox.radius
        );
        shapeRenderer.end();
    }

    public void activateState(int state) {
        this.objectState |= state;
    }

    public void removeState(int state) {
        this.objectState &= ~state;
    }

    public Texture getTexture() {
        return textures[(int) animationState];
    }


}
