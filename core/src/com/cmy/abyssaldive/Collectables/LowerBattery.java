package com.cmy.abyssaldive.Collectables;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Circle;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.Player.Submarine;
import com.cmy.abyssaldive.SubmarineGame;

import java.util.Random;

import static com.badlogic.gdx.math.MathUtils.map;

public class LowerBattery extends Collectible {
    public static final float DEFAULT_COOLDOWN_SPAWN_NEW_BATTERY = 20f;
    public Sound lowerBattery;

    public LowerBattery(com.cmy.abyssaldive.Assets assets, SubmarineGame submarineGame) {
        super(assets, submarineGame);

        lowerBattery = assets.manager.get(com.cmy.abyssaldive.Assets.lowerBattery);

        Random random = new Random();

        textures = new Texture[]{
                assets.manager.get(com.cmy.abyssaldive.Assets.lowerBattery1),
                assets.manager.get(Assets.lowerBattery2)
        };


        width = 175f;
        height = 175f;
        x = map(0f, 1f, 50f, 1000f, random.nextFloat());//value x will be between 50 to 1000
        y = -height;

        animationSpeed = 5f;
        movementSpeedY = 600f;
        hitboxScale = 0.95f;
        rotation = 0f;
        animationState = 0;
        finalAnimationState = textures.length;

        hitbox = new Circle(
                x + width / 2,
                y + height / 2,
                hitboxScale * width / 2
        );

        objectState = ALIVE | CAN_INTERACT_WITH_PLAYER | VISIBLE | MOVING;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
    }

    @Override
    public void applyEffectToPlayer(Submarine submarine) {
        submarine.increaseBattery(25);
        lowerBattery.play(submarineGame.audioVolume);
        this.removeState(ALIVE);
    }

}

