package com.cmy.abyssaldive.Enemies;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.Player.Submarine;
import com.cmy.abyssaldive.SubmarineGame;

import java.util.Random;

public class ExplosiveMine extends Enemy {
    public static final int DEFAULT_WIDTH = 150;
    public static final int DEFAULT_HEIGHT = 150;
    public static final int DEFAULT_INITIAL_STATE = ALIVE | VISIBLE | MOVING | CAN_INTERACT_WITH_PLAYER | ROTATING;
    private static final float DEFAULT_TIME_SPAWN_MINE = 5f;
    private static float cooldownSpawnMines = DEFAULT_TIME_SPAWN_MINE;
    public Sound mineExplosionSound;

    public ExplosiveMine(Texture[] textures,
                         Texture[] dyingTextures,
                         float x,
                         float y,
                         float width,
                         float height,
                         int objectState,
                         com.cmy.abyssaldive.SubmarineGame submarineGame) {
        super(textures, dyingTextures, x, y, width, height, objectState, submarineGame);
    }

    public static ExplosiveMine generateRandomExplosiveMine(com.cmy.abyssaldive.Assets assets, float currentDepth, float deltaTime, com.cmy.abyssaldive.SubmarineGame submarineGame) {
        if (currentDepth < 100)
            return null;

        cooldownSpawnMines -= deltaTime;

        if (cooldownSpawnMines > 0)
            return null;

        cooldownSpawnMines = DEFAULT_TIME_SPAWN_MINE;

        Random random = new Random();

        float posX = random.nextInt(SubmarineGame.VIRTUAL_WIDTH - 20) + 10;

        Texture[] textures = new Texture[]{assets.manager.get(com.cmy.abyssaldive.Assets.navalMine)};
        Texture[] dyingTextures = new Texture[]{
                assets.manager.get(com.cmy.abyssaldive.Assets.navalMineExplosion01),
                assets.manager.get(com.cmy.abyssaldive.Assets.navalMineExplosion02),
                assets.manager.get(com.cmy.abyssaldive.Assets.navalMineExplosion03)
        };

        ExplosiveMine explosiveMine = new ExplosiveMine(
                textures,
                dyingTextures,
                posX,
                -DEFAULT_WIDTH,
                DEFAULT_WIDTH,
                DEFAULT_HEIGHT,
                DEFAULT_INITIAL_STATE,
                submarineGame
        );

        explosiveMine.mineExplosionSound = assets.manager.get(Assets.mineExplosionSound);

        return explosiveMine;
    }

    public static void resetCooldown() {
        cooldownSpawnMines = DEFAULT_TIME_SPAWN_MINE;
    }

    @Override
    public void dealDamage(com.cmy.abyssaldive.Player.Submarine submarine) {
        removeState(MOVING);
        removeState(CAN_INTERACT_WITH_PLAYER);
        addState(DYING);
        mineExplosionSound.play(submarineGame.audioVolume / 2.5f);
        submarine.removeLife(2);
    }

    @Override
    public void applyDebuff(Submarine submarine) {
        //no debuff from this enemy
    }
}
