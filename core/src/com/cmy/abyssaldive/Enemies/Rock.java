package com.cmy.abyssaldive.Enemies;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.cmy.abyssaldive.Assets;
import com.cmy.abyssaldive.Player.Submarine;
import com.cmy.abyssaldive.SubmarineGame;

import java.util.Random;

public class Rock extends Enemy {
    public static final int DEFAULT_WIDTH = 100;
    public static final int DEFAULT_HEIGHT = 100;
    public static final int DEFAULT_INITIAL_STATE = ALIVE | VISIBLE | MOVING | CAN_INTERACT_WITH_PLAYER | ROTATING;
    private static final float DEFAULT_TIME_SPAWN_ROCK = 1f;
    private static float cooldownSpawnRocks = DEFAULT_TIME_SPAWN_ROCK;
    public Sound rockBreakingSound;

    public Rock(Texture[] textures,
                Texture[] dyingTextures,
                float x,
                float y,
                float width,
                float height,
                int objectState,
                com.cmy.abyssaldive.SubmarineGame submarineGame) {
        super(textures, dyingTextures, x, y, width, height, objectState,submarineGame);
    }

    public static Rock generateRandomRock(com.cmy.abyssaldive.Assets assets, float currentDepth, float deltaTime, com.cmy.abyssaldive.SubmarineGame submarineGame) {
        if (currentDepth < 1)
            return null;

        cooldownSpawnRocks -= deltaTime;

        if (cooldownSpawnRocks > 0)
            return null;

        cooldownSpawnRocks = DEFAULT_TIME_SPAWN_ROCK - currentDepth / 1000;
        if (cooldownSpawnRocks < 0.2f)
            cooldownSpawnRocks = 0.2f;

        Random random = new Random();

        float randomScaling = (random.nextFloat() + 0.5f) * 1.5f;

        float width = randomScaling * DEFAULT_WIDTH;
        float height = randomScaling * DEFAULT_HEIGHT;
        float posX = random.nextInt(SubmarineGame.VIRTUAL_WIDTH - 20) + 10;
        float posY = -height;

        Texture[] textures = new Texture[]{assets.manager.get(com.cmy.abyssaldive.Assets.rock01)};
        Texture[] dyingTextures = new Texture[]{
                assets.manager.get(com.cmy.abyssaldive.Assets.rock01Broken01),
                assets.manager.get(com.cmy.abyssaldive.Assets.rock01Broken02),
                assets.manager.get(com.cmy.abyssaldive.Assets.rock01Broken03)
        };

        Rock rock = new Rock(
                textures,
                dyingTextures,
                posX,
                posY,
                width,
                height,
                DEFAULT_INITIAL_STATE,
                submarineGame
        );

        rock.rockBreakingSound = assets.manager.get(Assets.rockCollisionSound);
        rock.rotation = random.nextFloat() * 360;
        rock.rotationSpeed = random.nextFloat() * 10 + DEFAULT_ROTATION_SPEED;
        rock.movementSpeedY = random.nextFloat() * 200 + DEFAULT_MOVEMENT_SPEED_Y;

        return rock;
    }

    public static void resetCooldown() {
        cooldownSpawnRocks = DEFAULT_TIME_SPAWN_ROCK;
    }


    @Override
    public void dealDamage(com.cmy.abyssaldive.Player.Submarine submarine) {
        removeState(MOVING);
        removeState(CAN_INTERACT_WITH_PLAYER);
        addState(DYING);
        animationState = 0;
        rockBreakingSound.play(submarineGame.audioVolume / 2.5f);
        submarine.removeLife(1);
    }

    @Override
    public void applyDebuff(Submarine submarine) {
        //nothing
    }
}
