package com.cmy.abyssaldive.Enemies;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;
import com.cmy.abyssaldive.Player.Submarine;
import com.cmy.abyssaldive.SubmarineGame;

public abstract class Enemy {
    public static final int ALIVE = 0x01; //0000 0001
    public static final int CAN_INTERACT_WITH_PLAYER = 0x02; //0000 0010
    public static final int VISIBLE = 0x04; //0000 0100
    public static final int MOVING = 0x08; //0000 1000
    public static final int DYING = 0x10; //0001 0000
    public static final int ROTATING = 0x20; //0010 0000
    //obs.: int in java uses 32bits
    public static final float DEFAULT_ANIMATION_SPEED = 10f;
    public static final float DEFAULT_ROTATION_SPEED = 10f;
    public static final float DEFAULT_MOVEMENT_SPEED_Y = 500f;
    public static final float DEFAULT_HITBOX_SCALE = 0.95f;
    public final com.cmy.abyssaldive.SubmarineGame submarineGame;

    public int objectState;
    public float x;
    public float y;
    public float width;
    public float height;
    public Circle hitbox;
    public Texture[] textures;
    public Texture[] dyingTextures;
    public float movementSpeedY;
    public float animationState;
    public float animationSpeed;
    public float rotation;
    public float rotationSpeed;
    private float hitboxScale;

    public Enemy(Texture[] textures,
                 Texture[] dyingTextures,
                 float x,
                 float y,
                 float width,
                 float height,
                 int objectState,
                 com.cmy.abyssaldive.SubmarineGame submarineGame) {
        this.textures = textures;
        this.dyingTextures = dyingTextures;
        this.objectState = objectState;
        this.submarineGame = submarineGame;
        hitbox = new Circle(0, 0, 0);
        hitboxScale = DEFAULT_HITBOX_SCALE;
        movementSpeedY = DEFAULT_MOVEMENT_SPEED_Y;
        rotationSpeed = DEFAULT_ROTATION_SPEED;
        animationSpeed = DEFAULT_ANIMATION_SPEED;
        rotation = 0f;
        animationState = 0;

        setPosition(new Vector2(x, y));
        setSize(width, height);
    }

    /**
     * Defines the comportment of the enemy through the time variation deltaTIme
     *
     * @param deltaTime time variation between frames
     */
    public void update(float deltaTime) {
        if (isAlive()) {
            animationState += deltaTime * animationSpeed;
            if (isRotating()) {
                rotation += deltaTime * rotationSpeed;
            }
            if (isMoving()) {
                setPosition(new Vector2(x, y + deltaTime * movementSpeedY));
                if (y >= SubmarineGame.VIRTUAL_HEIGHT)
                    removeState(ALIVE);
            }

            if (isDying() && animationState >= dyingTextures.length) {
                removeState(ALIVE);
                animationState = 0;
            }

            if (!isDying() && animationState >= textures.length)
                animationState = 0;
        }
    }

    /**
     * Draws the enemy on a SpriteBatch that "begin()" was already been executed
     *
     * @param spriteBatch the SpriteBatch where the enemy will be drawn
     */
    public void draw(SpriteBatch spriteBatch) {
        if (isVisible()) {
            spriteBatch.begin();
            spriteBatch.setColor(1f, 1f, 1f, 1f);
            spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            spriteBatch.draw(
                    getTexture(),
                    x,
                    y,
                    width / 2,
                    height / 2,
                    width,
                    height,
                    1,
                    1,
                    rotation,
                    0,
                    0,
                    getTexture().getWidth(),
                    getTexture().getHeight(),
                    false,
                    false
            );
            spriteBatch.end();
        }
    }

    /**
     * Returns True if the Enemy hitbox is overlapping the Submarine hitbox
     *
     * @param submarine the submarine object that will have it hitbox checked against this enemy
     */
    public boolean isTouchingPlayer(com.cmy.abyssaldive.Player.Submarine submarine) {
        return Intersector.overlaps(hitbox, submarine.hitbox);

    }

    /**
     * Removes an certain ammount of life from the submarine
     *
     * @param submarine the submarine object so this can call submarine.removeLife()
     */
    public abstract void dealDamage(com.cmy.abyssaldive.Player.Submarine submarine);

    /**
     * Apply some special state to the submarine
     *
     * @param submarine so the special state can be applied
     */
    public abstract void applyDebuff(Submarine submarine);

    /**
     * Return true if the bit ALIVE (0x01) is equals 1
     */
    public boolean isAlive() {
        return (objectState & ALIVE) != 0;
    }

    /**
     * Return true if the bit DYING (0x10) is equals 1
     */
    public boolean isDying() {
        return (objectState & DYING) != 0;
    }

    /**
     * Return true if the bit MOVING (0x08) is equals 1
     */
    public boolean isMoving() {
        return (objectState & MOVING) != 0;
    }

    /**
     * Return true if the bit CAN_INTERACT_WITH_PLAYER (0x02) is equals 1
     */
    public boolean canInteractWithPlayer() {
        return (objectState & CAN_INTERACT_WITH_PLAYER) != 0;
    }

    /**
     * Return true if the bit VISIBLE (0x04) is equals 1
     */
    public boolean isVisible() {
        return (objectState & VISIBLE) != 0;
    }

    /**
     * Return true if the bit ROTATING (0x20) is equals 1
     */
    public boolean isRotating() {
        return (objectState & ROTATING) != 0;
    }

    /**
     * Draws the enemy hitbox on a ShapeRenderer that "begin()" was already been executed
     *
     * @param shapeRenderer the ShapeRenderer where the enemy will be drawn
     */
    public void drawHitbox(ShapeRenderer shapeRenderer) {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapeRenderer.setColor(new Color(1, 0, 0, 0.5f));
        shapeRenderer.circle(
                hitbox.x,
                hitbox.y,
                hitbox.radius
        );
        shapeRenderer.end();
    }

    /**
     * Returns the current texture of the enemy, based on the current animationState and objectState values
     */
    public Texture getTexture() {
        if (isDying()) {
            if (animationState < dyingTextures.length)
                return dyingTextures[(int) animationState];
            else
                return dyingTextures[0];
        } else {
            if (animationState < textures.length)
                return textures[(int) animationState];
            else
                return textures[0];
        }
    }

    public void addState(int state) {
        this.objectState |= state;
    }

    public void removeState(int state) {
        this.objectState &= ~state;
    }

    public void setPosition(Vector2 position) {
        this.x = position.x;
        this.y = position.y;
        hitbox.setX(position.x + width / 2);
        hitbox.setY(position.y + height / 2);
    }

    public void setSize(float width, float height) {
        this.width = width;
        this.height = height;
        hitbox.setX(x + width / 2);
        hitbox.setY(y + height / 2);
        hitbox.setRadius(width / 2 * hitboxScale);
    }

    public void setHitboxScale(float hitboxScale) {
        this.hitboxScale = hitboxScale;
        hitbox.setX(x + width / 2);
        hitbox.setY(y + height / 2);
        hitbox.setRadius(width / 2 * hitboxScale);
    }
}
