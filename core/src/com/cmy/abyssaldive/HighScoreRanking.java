package com.cmy.abyssaldive;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class HighScoreRanking {
    private final com.cmy.abyssaldive.SubmarineGame submarineGame;
    private Preferences highScores;
    private BitmapFont rankingFont;
    private Assets assets;
    private I18NBundle languageStrings;
    private float newScore;
    private Map<String, Float> highScoresMap;
    private TreeMap<String, Float> sortedMap;


    public HighScoreRanking(Preferences highScores, Assets assets, I18NBundle languageStrings, com.cmy.abyssaldive.SubmarineGame submarineGame) {
        this.highScores = highScores;
        this.submarineGame = submarineGame;
        this.assets = assets;
        this.languageStrings = languageStrings;

//        highScoresMap = (Map<String, Float>) highScores.get();

        rankingFont = assets.manager.get(Assets.regularsBitmapFont);

//        highScoresMap = updateRanking();
        highScoresMap = (Map<String, Float>) highScores.get();
        sortRanking();
    }


    public void draw(SpriteBatch spriteBatch) {
        spriteBatch.setColor(1f, 1f, 1f, 1f);
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.begin();

        rankingFont.getData().setScale(1f);
        rankingFont.draw(
                spriteBatch,
                "Score: " + (int) newScore,
                50,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.92f
        );

        rankingFont.getData().setScale(1.5f);
        rankingFont.draw(
                spriteBatch,
                languageStrings.get("high_score_result"),
                50,
                com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * 0.85f
        );

//        sortedMap.clear();
//
//        sortedMap.putAll(highScoresMap);
        rankingFont.getData().setScale(1f);
        int i = 0;
        for (Map.Entry<String, Float> entry : sortedMap.entrySet()) {
            if (i >= sortedMap.size() || i >= 10)
                break;
            rankingFont.draw(
                    spriteBatch,
                    entry.getValue().intValue() + " Depth",
                    50,
                    com.cmy.abyssaldive.SubmarineGame.VIRTUAL_HEIGHT * (0.8f - (i + 1) * 0.065f)
            );

            i++;
        }

        rankingFont.draw(
                spriteBatch,
                "Total: " + (int) submarineGame.totalScore,
                50,
                SubmarineGame.VIRTUAL_HEIGHT * (0.8f - (i + 1) * 0.065f)
        );

        spriteBatch.end();
    }

    public Map<String, Float> updateRanking(float newScore) {
        //Save the new score on the file
        submarineGame.totalScore = sumScores(highScoresMap, newScore);
        highScores.putFloat("totalScores", submarineGame.totalScore);
        highScores.putFloat(Calendar.getInstance().getTime().toString(), newScore);
        highScores.flush();

        this.newScore = newScore;

        highScoresMap = (Map<String, Float>) highScores.get();
        if (highScoresMap.containsKey("totalScores")) {
            highScoresMap.remove("totalScores");
        }
        return highScoresMap;
    }

    public TreeMap<String, Float> sortRanking() {
        ValueComparator valueComparator = new ValueComparator(highScoresMap);
        sortedMap = new TreeMap<String, Float>((Comparator) valueComparator);
        sortedMap.clear();
        sortedMap.putAll(highScoresMap);

        return sortedMap;
    }

    private float sumScores(Map<String, Float> map, float newScore) {
        float sum = 0;

        for (Map.Entry<String, Float> entry : map.entrySet()) {
            if (!entry.getKey().equals("totalScores"))
                sum += entry.getValue();
        }

        sum += newScore;

        return sum;
    }

    private class ValueComparator implements Comparator<String> {
        Map<String, Float> base;

        ValueComparator(Map<String, Float> base) {
            this.base = base;
        }

        @Override
        public int compare(String a, String b) {
            if (base.get(a) > base.get(b)) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
