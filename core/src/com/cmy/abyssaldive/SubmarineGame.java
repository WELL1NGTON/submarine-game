package com.cmy.abyssaldive;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.cmy.abyssaldive.Menus.InstructionsMenu;
import com.cmy.abyssaldive.Collectables.Battery;
import com.cmy.abyssaldive.Collectables.Collectible;
import com.cmy.abyssaldive.Collectables.Life;
import com.cmy.abyssaldive.Collectables.LowerBattery;
import com.cmy.abyssaldive.Enemies.Enemy;
import com.cmy.abyssaldive.Enemies.ExplosiveMine;
import com.cmy.abyssaldive.Enemies.Rock;
import com.cmy.abyssaldive.Menus.MainMenu;
import com.cmy.abyssaldive.Menus.SettingsMenu;
import com.cmy.abyssaldive.Player.Submarine;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class SubmarineGame extends ApplicationAdapter {
    public static final int VIRTUAL_WIDTH = 1080;
    public static final int VIRTUAL_HEIGHT = 1920;
    public static final int GAME_STATE_MAIN_MENU = 0;
    public static final int GAME_STATE_RUNNING = 1;
    public static final int GAME_STATE_DEAD = 2;
    public static final int GAME_STATE_LOADING = 3;
    public static final int GAME_STATE_SETTINGS_MENU = 4;
    public static final int GAME_STATE_INSTRUCTIONS_MENU = 5;
    public static final float DEFAULT_DESCENT_SPEED = 4f;

    public Assets assets;
    public int gameState;
    public MainMenu mainMenu;
    public com.cmy.abyssaldive.Background background;
    public com.cmy.abyssaldive.UserInterface userInterface;
    public SettingsMenu settingsMenu;
    public com.cmy.abyssaldive.Menus.InstructionsMenu instructionsMenu;
    public float audioVolume;
    public float totalScore;
    public float highScore;
    public Locale locale;
    public Submarine submarine;
    public int submarineType;
    public Sound gameMusic;
    private OrthographicCamera camera;
    private Viewport viewport;
    private SpriteBatch spriteBatch;
    private ShapeRenderer shapeRenderer;
    private Texture loadingScreen;
    private float depthMeter;
    private float deltaTime;
    private float ambientLightLevel;
    private HighScoreRanking highScoreRanking;
    private LinkedList<Enemy> enemyList = new LinkedList<>();
    private LinkedList<Collectible> collectibleList = new LinkedList<>();
    private I18NBundle languageStrings;
    private boolean debugMode = false;
    private Preferences highScores;
    private Preferences preferences;
    private Circle[] touchsVisualDebug;
    private float cooldownSpawnNuclearBattery;
    private float cooldownSpawnLife;
    private float cooldownSpawnLowerBattery;
    private boolean moveSubmarine;

    @Override
    public void create() {
        assets = new Assets();
        spriteBatch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();

        assets.loadLoadingScreen();
        assets.manager.finishLoading();


        loadingScreen = assets.manager.get(Assets.loadingScreen);

        camera = new OrthographicCamera();
        camera.position.set(VIRTUAL_WIDTH >> 1, VIRTUAL_HEIGHT >> 1, 0);
        viewport = new FitViewport(VIRTUAL_WIDTH, VIRTUAL_HEIGHT, camera);
        depthMeter = 0;

        assets.loadBackgrounds();
        assets.loadSubmarineTextures();
        assets.loadEnemies();
        assets.loadSounds();
        assets.loadFonts();
        assets.loadUserInterface();
        assets.loadCollectibles();

        ambientLightLevel = 1f;

        gameState = GAME_STATE_LOADING;

        highScores = Gdx.app.getPreferences("highScores");
        preferences = Gdx.app.getPreferences("gamePreferences");

//        highScores.clear();
//        highScores.flush();

        if (preferences.get().size() != 0 && preferences.contains("country") && preferences.contains("language")) {
            String country = preferences.getString("country");
            String language = preferences.getString("language");
            locale = new Locale(language, country);

            Gdx.app.log("locale", "language: " + locale.getLanguage());
            Gdx.app.log("locale", "country: " + locale.getCountry());
        } else {
            locale = Locale.getDefault();
            preferences.putString("country", locale.getCountry());
            preferences.putString("language", locale.getLanguage());

            Gdx.app.log("locale", "default");
        }


        if (highScores.get().size() == 0 || !highScores.contains("totalScores")) {
            highScores.putFloat("totalScores", 0f);
            totalScore = 0;
            preferences.flush();
        } else {
            totalScore = highScores.getFloat("totalScores");

            Map<String, Float> highScoresMap = (Map<String, Float>) highScores.get();

            for (Map.Entry<String, Float> entry : highScoresMap.entrySet()) {
                if (!entry.getKey().equals("totalScores")) {
                    if (entry.getValue() > highScore)
                        highScore = entry.getValue();
                }
            }
        }

        if (preferences.contains("audioVolume")) {
            audioVolume = preferences.getFloat("audioVolume");
        } else {
            preferences.putFloat("audioVolume", 1f);
            audioVolume = 1f;
        }

        if (preferences.contains("submarineType")) {
            submarineType = preferences.getInteger("submarineType");
        } else {
            submarineType = Submarine.SUBMARINE_TYPE_DEFAULT;
            preferences.putInteger("submarineType", Submarine.SUBMARINE_TYPE_DEFAULT);
        }

        preferences.flush();
        highScores.flush();

        languageStrings = I18NBundle.createBundle(Gdx.files.internal("strings/strings"), locale);

        if (debugMode) {
            touchsVisualDebug = new Circle[]{
                    new Circle(-1000, -1000, 150),
                    new Circle(-1000, -1000, 150),
                    new Circle(-1000, -1000, 150),
                    new Circle(-1000, -1000, 150),
                    new Circle(-1000, -1000, 150)
            };
        }
        cooldownSpawnLife = Battery.DEFAULT_COOLDOWN_SPAWN_NEW_BATTERY;
    }

    @Override
    public void render() {
        camera.update();

        deltaTime = Gdx.graphics.getDeltaTime();

        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteBatch.setProjectionMatrix(camera.combined);

        switch (gameState) {
            case GAME_STATE_MAIN_MENU:
                //updates deltaTime
                mainMenu.update(deltaTime);
                background.update(deltaTime);

                //draw background and menu
                background.draw(spriteBatch);
                mainMenu.draw();

                break;
            case GAME_STATE_SETTINGS_MENU:
                //updates deltaTime
                settingsMenu.update(deltaTime);
                background.update(deltaTime);

                //draw background and menu
                background.draw(spriteBatch);
                settingsMenu.draw(spriteBatch);
                break;
            case GAME_STATE_INSTRUCTIONS_MENU:
                //updates deltaTime
                instructionsMenu.update(deltaTime);
                background.update(deltaTime);

                //draw background and menu
//                background.draw(spriteBatch);
                instructionsMenu.draw(spriteBatch);
                break;
            case GAME_STATE_RUNNING:
                if (submarine.state == Submarine.STATE_DEAD) {
                    gameMusic.stop();

                    //update the highscores screen to include the new score
                    highScoreRanking.updateRanking(depthMeter);
                    highScoreRanking.sortRanking();

                    //set game to dead state
                    gameState = GAME_STATE_DEAD;
                } else {
                    //get userInput
                    int movementDirection = Submarine.SUBMARINE_STOPPED;
                    if (Gdx.input.isTouched()) {
                        Vector2 touchPos = new Vector2(0, 0);
                        for (int i = 0; i < 5; i++) {
                            if (Gdx.input.isTouched(i)) {
                                int x = Gdx.input.getX(i);
                                int y = Gdx.input.getY(i);
                                touchPos.set(x, y);
                                viewport.unproject(touchPos);

                                if (debugMode) {
                                    touchsVisualDebug[i].x = touchPos.x;
                                    touchsVisualDebug[i].y = touchPos.y;
                                    touchsVisualDebug[i].radius = Gdx.input.getPressure(i) * 150;
                                    Gdx.app.log("touch " + i, "x = " + touchPos.x + " | y = " + touchPos.y);
                                }
                            }
                        }

                        if (moveSubmarine) {
                            if (Math.abs(touchPos.x - (submarine.x + submarine.width / 2)) < 5) {
                                moveSubmarine = false;
                            }
                            if (touchPos.x > submarine.x + submarine.width / 2)
                                movementDirection = Submarine.MOVE_SUBMARINE_RIGHT;
                            else
                                movementDirection = Submarine.MOVE_SUBMARINE_LEFT;
                        } else {
                            if (Math.abs(touchPos.x - (submarine.x + submarine.width / 2)) > 20) {
                                moveSubmarine = true;
                            }
                        }

                    }

                    //spawn things
                    spawnEnemies();
                    spawnLoot();

                    //update things with deltaTime
                    submarine.update(deltaTime, movementDirection);
                    background.update(deltaTime);
                    //update Enemies
                    for (Iterator<Enemy> iterator = enemyList.iterator(); iterator.hasNext(); ) {
                        Enemy enemy = iterator.next();
                        enemy.update(deltaTime);
                        if (!enemy.isAlive())
                            iterator.remove();
                    }
                    //update Collectibles
                    for (Iterator<Collectible> iterator = collectibleList.iterator(); iterator.hasNext(); ) {
                        Collectible collectible = iterator.next();
                        collectible.update(deltaTime);
                        if (!collectible.isAlive())
                            iterator.remove();
                    }


                    //update the depth
                    depthMeter += deltaTime * DEFAULT_DESCENT_SPEED;

                    //update the ambient light based on the depth
                    float minLight = 0.01f;
                    if (submarine.battery >= 75)
                        minLight = 0.05f;
                    else if (submarine.battery >= 50)
                        minLight = 0.025f;
                    if (depthMeter > 100 - minLight*100)
                        ambientLightLevel = minLight; //(190 / -500f) + 1
                    else
                        ambientLightLevel = depthMeter / -100 + 1;

                    //check for collisions with the submarin    e (Enemies and Loot)
                    checkCollisionsSubmarine();
                }

                //draw the game screen
                drawGame();

                //show debug information
                if (debugMode) {
                    Gdx.app.log("deltaTime: ", "" + deltaTime);
                    Gdx.app.log("FPS: ", "" + (1 / deltaTime));
                    drawHitboxes();
                }

                break;
            case GAME_STATE_DEAD:
                if (Gdx.input.justTouched()) {
                    resetGame();
                    goToMainMenu();
                }

//                drawGame();

                highScoreRanking.draw(spriteBatch);


                break;
            case GAME_STATE_LOADING:
                if (assets.manager.update()) {
                    mainMenu = new MainMenu(assets, languageStrings, viewport, this, audioVolume);
                    mainMenu.setInputStageToThis();
                    settingsMenu = new SettingsMenu(assets, languageStrings, viewport, this, preferences);
                    instructionsMenu = new com.cmy.abyssaldive.Menus.InstructionsMenu(assets, languageStrings, viewport, this, preferences);
                    gameMusic = assets.manager.get(Assets.underwaterSound);
                    resetGame();
                    goToMainMenu();
                }
                drawLoadScreen(assets.manager.getProgress());
                break;
            default:
                break;
        }


    }

    private void drawLoadScreen(float progress) {
        Gdx.app.log("Loading: ", ((int) (progress * 100)) + "% carregado!");

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.rect(
                90,
                VIRTUAL_HEIGHT * 0.5f - 50,
                progress * 900,
                2 * 50
        );
        shapeRenderer.end();

        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.setColor(1f, 1f, 1f, 1f);
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.begin();
        spriteBatch.draw(loadingScreen, 0, 0);
        spriteBatch.end();


    }

    private void checkCollisionsSubmarine() {
        for (Enemy enemy : enemyList) {
            if (enemy.canInteractWithPlayer()) {
                if (enemy.isTouchingPlayer(submarine)) {
                    enemy.dealDamage(submarine);
                }
            }
        }
        for (Collectible collectible : collectibleList) {
            if (collectible.canInteractWithPlayer()) {
                if (collectible.isTouchingPlayer(submarine)) {
                    collectible.applyEffectToPlayer(submarine);
                }
            }
        }
    }

    private void drawGame() {
        spriteBatch.setProjectionMatrix(camera.combined);

        background.draw(spriteBatch);

        for (Enemy enemy : enemyList) {
            enemy.draw(spriteBatch);
        }

        submarine.submarineLight.draw(spriteBatch, ambientLightLevel);

        for (Collectible collectible : collectibleList) {
            collectible.draw(spriteBatch);
        }

        submarine.draw(spriteBatch);

        userInterface.draw(spriteBatch, submarine, depthMeter);
    }

    public void spawnEnemies() {
        ExplosiveMine explosiveMine = ExplosiveMine.generateRandomExplosiveMine(assets, depthMeter, deltaTime, this);
        if (explosiveMine != null)
            enemyList.add(explosiveMine);

        Rock rock = Rock.generateRandomRock(assets, depthMeter, deltaTime, this);
        if (rock != null)
            enemyList.add(rock);
    }

    public void spawnLoot() {
        Random random = new Random();
        cooldownSpawnNuclearBattery -= deltaTime;
        cooldownSpawnLowerBattery -= deltaTime;
        cooldownSpawnLife -= deltaTime;

        if (cooldownSpawnNuclearBattery < 0) {
            cooldownSpawnNuclearBattery = Battery.DEFAULT_COOLDOWN_SPAWN_NEW_BATTERY + random.nextFloat() * 20; //one Battery every 50~70 seconds
            collectibleList.add(new Battery(assets, this));
        }


        if (cooldownSpawnLowerBattery < 0) {
            cooldownSpawnLowerBattery = LowerBattery.DEFAULT_COOLDOWN_SPAWN_NEW_BATTERY + random.nextFloat() * 10; //one Battery every 20~30 seconds
            collectibleList.add(new LowerBattery(assets, this));
        }


//        if (cooldownSpawnLife < 0) {
//            cooldownSpawnLife = map( //one Life every DefaultTime plus or less 10% seconds
//                    0f,
//                    1f,
//                    Life.DEFAULT_COOLDOWN_SPAWN_NEW_LIFE * 0.9f,
//                    Life.DEFAULT_COOLDOWN_SPAWN_NEW_LIFE * 1.1f,
//                    random.nextFloat()
//            );
////                    Life.DEFAULT_COOLDOWN_SPAWN_NEW_LIFE + random.nextFloat()*5; //one Life every 10~15 seconds
////            collectibleList.add(new Life(assets, this));
//        }
    }


    public void resetGame() {
        preferences = Gdx.app.getPreferences("gamePreferences");

        if (preferences.get().size() != 0 && preferences.contains("country") && preferences.contains("language")) {
            String country = preferences.getString("country");
            String language = preferences.getString("language");
            locale = new Locale(language, country);

            Gdx.app.log("locale", "language: " + locale.getLanguage());
            Gdx.app.log("locale", "country: " + locale.getCountry());
        } else {
            locale = Locale.getDefault();
            preferences.putString("country", locale.getCountry());
            preferences.putString("language", locale.getLanguage());
            preferences.flush();

            Gdx.app.log("locale", "default");
        }

        languageStrings = I18NBundle.createBundle(Gdx.files.internal("strings/strings"), locale);

        background = new com.cmy.abyssaldive.Background(assets);
        background.setBackground(assets.manager.get(Assets.menuBackground), com.cmy.abyssaldive.Background.STATIC_BACKGROUND, com.cmy.abyssaldive.Background.TRANSITION_BACKGROUNDS_ABRUPT);
        settingsMenu.stage.dispose();
        settingsMenu = new SettingsMenu(assets, languageStrings, viewport, this, preferences);
        instructionsMenu = new InstructionsMenu(assets, languageStrings, viewport, this, preferences);
        mainMenu.stage.dispose();
        mainMenu.stopMenuMusic();
        mainMenu = new MainMenu(assets, languageStrings, viewport, this, audioVolume);

        submarine = new Submarine(assets, submarineType);


        userInterface = new UserInterface(assets, viewport, camera);
        highScoreRanking = new HighScoreRanking(highScores, assets, languageStrings, this);

        //Reset depth
        depthMeter = 0;

        //Clear Entities
        enemyList.clear();
        collectibleList.clear();

        //Reset Player stats
        submarine.reset();

        //Reset cooldowns
        cooldownSpawnNuclearBattery = Battery.DEFAULT_COOLDOWN_SPAWN_NEW_BATTERY;
        cooldownSpawnLowerBattery = LowerBattery.DEFAULT_COOLDOWN_SPAWN_NEW_BATTERY;
        Rock.resetCooldown();
        ExplosiveMine.resetCooldown();
        cooldownSpawnLife = Life.DEFAULT_COOLDOWN_SPAWN_NEW_LIFE;
    }

    public void goToMainMenu() {
        background.setBackground(assets.manager.get(Assets.menuBackground), com.cmy.abyssaldive.Background.STATIC_BACKGROUND, Background.TRANSITION_BACKGROUNDS_ABRUPT);
        mainMenu.playMenuMusic();
        mainMenu.setInputStageToThis();
        gameState = GAME_STATE_MAIN_MENU;
    }


    @Override
    public void dispose() {
        spriteBatch.dispose();
//        mainMenu.stage.dispose();
//        userInterface.stage.dispose();
        shapeRenderer.dispose();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    private void drawHitboxes() {
        shapeRenderer.setProjectionMatrix(camera.combined);

        for (Enemy enemy : enemyList) enemy.drawHitbox(shapeRenderer);

        for (Collectible collectible : collectibleList) collectible.drawHitbox(shapeRenderer);


        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.GREEN);
        shapeRenderer.rect(submarine.hitbox.x, submarine.hitbox.y, submarine.hitbox.width, submarine.hitbox.height);
        shapeRenderer.setColor(Color.BLUE);
        for (Circle circle : touchsVisualDebug) {
            shapeRenderer.circle(circle.x, circle.y, circle.radius);
            circle.x = -1000;
            circle.y = -1000;
        }
        shapeRenderer.end();
    }
}
